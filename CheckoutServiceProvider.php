<?php

namespace Kiberzauras\Checkout;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class CheckoutServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->setupRoutes($this->app->router);

        $this->publishes([
            __DIR__.'/config.php' => config_path('checkout.php'),
        ], 'config');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * @param Router $router
     */
    private function setupRoutes(Router $router)
    {
        if (! $this->app->routesAreCached()) {
            $router->group(['namespace' => 'Kiberzauras\Checkout\Http\Controllers'], function($router) {
                require __DIR__.'/Http/routes.php';
            });
        }
    }

}
