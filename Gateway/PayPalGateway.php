<?php

namespace Kiberzauras\Checkout\Gateway;

use Illuminate\Http\Request;
use Kiberzauras\Checkout\Lib\CheckoutException;
use Kiberzauras\Checkout\Lib\Gateway;
use Kiberzauras\Checkout\Lib\Mode;
use Kiberzauras\Checkout\Lib\Provider;

/**
 * Class PayPalGateway
 * @package Kiberzauras\Checkout\Gateway
 */
class PayPalGateway {

    /**
     * @var object
     */
    protected $request;
    protected $username;
    protected $token;
    protected $signature;
    protected $mode;

    public function __construct($request)
    {
        $this->request = $request = Gateway::parseRequest($request);

        if ($this->request->provider != Provider::PAYPAL)
            throw new CheckoutException('Bad gateway');

        $config = Gateway::getCredentials($this->request->provider, $this->request->mode);

        $this->username  = $config['username'];
        $this->password  = $config['password'];
        $this->signature = $config['signature'];
        $this->mode      = $this->request->mode == Mode::LIVE ? 'live' : 'sandbox';
    }

    public function startPayment()
    {
        //
    }

    public function processRequest(Request $http)
    {
        //
    }
}
