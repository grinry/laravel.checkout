<?php

namespace Kiberzauras\Checkout\Gateway;

use Illuminate\Http\Request;
use Kiberzauras\Checkout\Lib\Callback;
use Kiberzauras\Checkout\Lib\CheckoutException;
use Kiberzauras\Checkout\Lib\Gateway;
use Kiberzauras\Checkout\Lib\Provider;
use Kiberzauras\Checkout\Lib\TransactionData;

/**
 * Class PaySeraGateway
 * @package Kiberzauras\Checkout\Gateway
 */
class PaySeraGateway {

    /**
     * @var object
     */
    protected $request;
    protected $projectId;
    protected $signPassword;
    protected $accept;
    protected $mode;

    /**
     * @param $request
     * @throws CheckoutException
     */
    public function __construct($request)
    {
        $this->request = $request = Gateway::parseRequest($request);

        if ($this->request->provider != Provider::PAYSERA)
            throw new CheckoutException('Bad gateway');

        $config = Gateway::getCredentials($this->request->provider, $this->request->mode);

        $this->projectId    = $config['projectId'];
        $this->signPassword = $config['signPassword'];
        $this->accept   = $config['accept'];
        $this->mode     = $this->request->mode;
    }

    /**
     * @throws CheckoutException
     */
    public function startPayment()
    {
        $total_amount = ($this->request->amount + $this->request->tax_amount - $this->request->discount_amount) * 100;
        if ($total_amount <= 0)
            throw new CheckoutException('Amount (price) must be greater than zero (Check your discounts)');

        try {
            \WebToPay::redirectToPayment(array(
                'projectid'     => $this->projectId,
                'sign_password' => $this->signPassword,
                'orderid'       => $this->request->transaction,
                'amount'        => $total_amount,
                'currency'      => $this->request->currency,
                'accepturl'     => $this->request->accept_url,
                'cancelurl'     => $this->request->cancel_url,
                'callbackurl'   => route('checkout::callback', [
                    'transaction' => $this->request->transaction
                ]),
                'test'          => $this->mode,
                'paytext'       =>  $this->request->description ? $this->request->description
                    . ' ([order_nr], [site_name])' : null,

                'p_firstname'   =>  $this->request->customer_data->first_name,
                'p_lastname'    =>  $this->request->customer_data->last_name,
                'p_personcode'  =>  $this->request->customer_data->person_code,
                'p_email'  =>  $this->request->customer_data->email,
                'p_street'  =>  $this->request->customer_data->street,
                'p_city'  =>  $this->request->customer_data->city,
                'p_state'  =>  $this->request->customer_data->state,
                'p_zip'  =>  $this->request->customer_data->zip_code,
                'p_countrycode'  =>  $this->request->customer_data->country_code,
            ));
        } catch (\WebToPayException $e) {
            throw new CheckoutException('Gateway had error.', 500, $e);
        }
    }

    /**
     * @param Request $http
     * @throws CheckoutException
     */
    public function processRequest(Request $http)
    {
        try {
            $response = \WebToPay::checkResponse($http->all(), array(
                'projectid'     => $this->projectId,
                'sign_password' => $this->signPassword,
            ));

            if ($response['test'] !== '0') {
                //throw new CheckoutException('Testing, real payment was not made');
            }

            if ($response['type'] !== 'macro') {
                throw new CheckoutException('Only macro payment callbacks are accepted');
            }

            if ($response['orderid'] != $this->request->transaction)
                throw new CheckoutException('Incorrect transaction');

            if ($response['amount'] != $this->request->amount * 100)
                throw new CheckoutException('Incorrect amount');

            if ($response['currency'] != $this->request->currency)
                throw new CheckoutException('Incorrect currency');

            if ($this->request->status == TransactionData::PAID)
                throw new CheckoutException('Transaction already done.');

            TransactionData::update($this->request->transaction, [
                'date_paid' =>  time(),
                'status'    =>  TransactionData::PAID,
                'gateway_response' => $response
            ]);

            Callback::call($this->request);
            echo 'OK';
        } catch (CheckoutException $e) {
            throw new CheckoutException('Gateway got error', 500, $e);
        }
    }
}
