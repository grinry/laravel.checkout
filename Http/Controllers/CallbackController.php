<?php

namespace Kiberzauras\Checkout\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Kiberzauras\Checkout\Lib\CheckoutException;
use Kiberzauras\Checkout\Lib\Provider;
use Kiberzauras\Checkout\Lib\TransactionData;

class CallbackController extends Controller {

    public function index($transactionId, Request $request)
    {
        $transaction = TransactionData::get($transactionId);

        if (empty($transaction->transaction))
            throw new CheckoutException('No transaction found!');

        $gatewayClass = '\\Kiberzauras\\Checkout\\Gateway\\'. Provider::getGatewayName($transaction->provider) . "Gateway";
        (new $gatewayClass($transaction))
            ->processRequest($request);
    }//index

}//CallbackController
