<?php

Route::group(['prefix'=>'checkout', 'as'=>'checkout::'], function() {
    Route::get('/', ['as'=>'home', function() {
        return 'hey';
    }]);

    Route::any('callback/{transaction}', ['as'=>'callback', 'uses'=>'CallbackController@index']);
});