<?php

namespace Kiberzauras\Checkout\Lib;
/**
 * Class CheckoutException
 * @package Kiberzauras\Checkout\Lib
 */
class CheckoutException extends \Exception {
    public function __construct($message, $code = 0, \Exception $exception = null) {
        parent::__construct($message, $code, $exception);
    }
}
