<?php

namespace Kiberzauras\Checkout\Lib;

/**
 * Class Customer
 * @package Kiberzauras\Checkout\Lib
 */
class Customer {

    protected $first_name;
    protected $last_name;
    protected $person_code;
    protected $email;
    protected $phone;
    protected $street;
    protected $city;
    protected $state;
    protected $zip_code;
    protected $country_code;
    protected $optionals = [];

    /**
     * @param null $first_name
     * @param null $last_name
     * @param null $person_code
     * @param null $email
     */
    public function __construct($first_name = null, $last_name = null, $person_code = null, $email = null)
    {
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->person_code = $person_code;
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function expose()
    {
        return get_object_vars($this);
    }

    /**
     * @param mixed $email
     * @return Customer
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @param mixed $phone
     * @return Customer
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @param string|array $optionals
     * @return Customer
     */
    public function setOptionals($optionals)
    {
        $this->optionals = (array) $optionals;
        return $this;
    }

    /**
     * @param mixed $country_code
     * @return Customer
     */
    public function setCountryCode($country_code)
    {
        $this->country_code = $country_code;
        return $this;
    }

    /**
     * @param mixed $first_name
     * @return Customer
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
        return $this;
    }

    /**
     * @param mixed $last_name
     * @return Customer
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
        return $this;
    }

    /**
     * @param mixed $person_code
     * @return Customer
     */
    public function setPersonCode($person_code)
    {
        $this->person_code = $person_code;
        return $this;
    }

    /**
     * @param mixed $city
     * @return Customer
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @param mixed $street
     * @return Customer
     */
    public function setStreet($street)
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @param mixed $state
     * @return Customer
     */
    public function setState($state)
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @param mixed $zip_code
     * @return Customer
     */
    public function setZipCode($zip_code)
    {
        $this->zip_code = $zip_code;
        return $this;
    }
}
