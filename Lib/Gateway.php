<?php

namespace Kiberzauras\Checkout\Lib;

/**
 * Class Gateway
 * @package Kiberzauras\Checkout\Lib
 */
class Gateway
{

    /**
     * @param $request
     * @return object
     */
    public static function parseRequest($request)
    {
        return (object) $request;
    }

    /**
     * @param $provider
     * @param $mode
     * @return mixed
     * @throws CheckoutException
     */
    public static function getCredentials($provider, $mode)
    {
        if ($mode == Mode::SANDBOX) {
            $array = config('checkout.sandbox_providers');
            if (array_key_exists($provider, $array))
                return $array[$provider];
        }

        //If mode is live or sandbox but sandbox doesn't have provider fallback to live providers
        $array = config('checkout.providers');
        if (array_key_exists($provider, $array))
            return $array[$provider];

        throw new CheckoutException('Cant find selected provider in config file');
    }

}
