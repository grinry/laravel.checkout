<?php
namespace Kiberzauras\Checkout\Lib;

class Mode {
    const LIVE = 0;
    const SANDBOX = 1;
}
