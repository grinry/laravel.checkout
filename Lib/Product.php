<?php

namespace Kiberzauras\Checkout\Lib;

/**
 * Class Product
 * @package Kiberzauras\Checkout\Lib
 */
class Product {

    protected $product_id;
    protected $amount = 0.00;
    protected $quantity = 0.00;
    protected $measure = null;
    protected $name = null;
    protected $description = null;
    protected $tax_rate = 0.00;
    protected $tax_value = 0.00;
    protected $discount_rate = 0.00;
    protected $discount_value = 0.00;
    protected $optional = array();

    /**
     * @param null $name
     * @param float $amount
     * @param float $quantity
     * @param null $measure
     */
    public function __construct($name = null, $amount = 0.00, $quantity = 1.00, $measure = null)
    {
        $this->name = $name;
        $this->amount = $amount;
        $this->quantity = $quantity;
        $this->measure = $measure;
    }

    /**
     * @param $product_id
     * @return $this
     */
    public function setProductId($product_id)
    {
        $this->product_id = $product_id;
        return $this;
    }

    /**
     * @param float $amount
     * @return $this
     */
    public function setAmount($amount = 0.00)
    {
        $this->amount = is_numeric($amount) ? $amount : 0.00;
        return $this;
    }

    /**
     * @param float $quantity
     * @return $this
     */
    public function setQuantity($quantity = 1.00)
    {
        $this->quantity = is_numeric($quantity) ? $quantity : 1.00;
        return $this;
    }

    /**
     * @param null $measure
     * @return $this
     */
    public function setMeasure($measure = null)
    {
        $this->measure = $measure;
        return $this;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @param $rate
     * @return $this
     */
    public function setTaxRate($rate)
    {
        $this->tax_rate = is_numeric($rate) ? $rate : 0.00;
        return $this;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setTaxValue($value)
    {
        $this->tax_value = is_numeric($value) ? $value : 0.00;
        return $this;
    }

    /**
     * @param $rate
     * @return $this
     */
    public function setDiscountRate($rate)
    {
        $this->discount_rate = is_numeric($rate) ? $rate : 0.00;
        return $this;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setDiscountValue($value)
    {
        $this->discount_value = is_numeric($value) ? $value : 0.00;
        return $this;
    }

    /**
     * @param $optional
     * @return $this
     */
    public function setOptional($optional)
    {
        $this->optional = (array)$optional;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return float
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @return null
     */
    public function getMeasure()
    {
        return $this->measure;
    }

    /**
     * @return null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return float
     */
    public function getTaxRate()
    {
        return $this->tax_rate;
    }

    /**
     * @return float
     */
    public function getTaxValue()
    {
        return $this->tax_value;
    }

    /**
     * @return float
     */
    public function getDiscountRate()
    {
        return $this->discount_rate;
    }

    /**
     * @return float
     */
    public function getDiscountValue()
    {
        return $this->discount_value;
    }

    /**
     * @return array
     */
    public function getOptional()
    {
        return $this->optional;
    }

    /**
     * @return mixed
     */
    public function expose()
    {
        return get_object_vars($this);
    }
}
