<?php

namespace Kiberzauras\Checkout\Lib;
/**
 * Class Provider
 * @package Kiberzauras\Checkout\Lib
 */
class Provider {
    const PAYPAL = 0;
    const PAYSERA = 1;
    const BRAINTREE = 2;
    const GOOGLEWALLET = 3;

    /** List of supported gateways
     * @return array
     */
    public static function gatewayList()
    {
        return [
            self::PAYPAL    =>  'PayPal',
            self::PAYSERA    =>  'PaySera',
            self::BRAINTREE    =>  'BrainTree',
            self::GOOGLEWALLET    =>  'GoogleWallet',
        ];
    }

    /** List of gateways which supports subscriptions
     * @return array
     */
    public static function getSubscriptionProviders()
    {
        return [
            self::PAYPAL,
            self::BRAINTREE,
            self::GOOGLEWALLET
        ];
    }

    /**
     * @param $id
     * @return mixed
     * @throws CheckoutException
     */
    public static function getGatewayName($id)
    {
        $array = self::gatewayList();
        if (array_key_exists($id, $array)) {
            return $array[$id];
        }
        throw new CheckoutException('Provider does\'nt exist!');
    }


}
