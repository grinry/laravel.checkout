<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 2015.11.27
 * Time: 15:56
 */

namespace Kiberzauras\Checkout\Lib;

class TransactionData {

    const CREATED = 0;
    const PAID = 1;
    const WAITING = 2;

    /**
     * @param $request
     * @return mixed
     * @throws CheckoutException
     */
    public static function generate($request)
    {
        $data = $request;
        $data['transaction'] = uniqid();
        if (config('checkout.use_database') === true) {
            /**
             * @todo Add database support
             */
            throw new CheckoutException('Database is not supported yet.');
        } else {
            \Storage::prepend(config('checkout.table_prefix') . 'Transactions', $data['transaction'] . '|.'. json_encode($data));
        }
        return $data;
    }

    /**
     * @param $transaction
     * @return object
     * @throws CheckoutException
     */
    public static function get($transaction)
    {
        if (config('checkout.use_database') === true) {
            /**
             * @todo Add database support
             */
            throw new CheckoutException('Database is not supported yet.');
        } else {
            $file = \Storage::get(config('checkout.table_prefix') . 'Transactions');
            foreach (explode(PHP_EOL, $file) as $line) {
                $data = explode('|.', $line);
                if ($data[0] === $transaction) {
                    return json_decode($data[1]);
                    break;
                }
            }
            return (object) [];
        }
    }

    /**
     * @param $transaction
     * @param array $params
     * @throws CheckoutException
     */
    public static function update($transaction, $params = [])
    {
        if (config('checkout.use_database') === true) {
            /**
             * @todo Add database support
             */
            throw new CheckoutException('Database is not supported yet.');
        } else {
            $file = \Storage::get(config('checkout.table_prefix') . 'Transactions');
            $update = false;
            foreach (explode(PHP_EOL, $file) as $line) {
                $data = explode('|.', $line);
                if ($data[0] === $transaction) {
                    $json = json_decode($data[1]);
                    foreach ($params as $key=>$value) {
                        $json->{$key} = $value;
                    }
                    $json->updated = time();
                    $file = str_replace($line, $data[0] . '|.' . json_encode($json), $file);
                    $update = true;
                    break;
                }
            }

            if ($update)
               \Storage::put(config('checkout.table_prefix') . 'Transactions', $file);
        }
    }

    /**
     * @param $transaction
     * @throws CheckoutException
     */
    public static function remove($transaction)
    {
        if (config('checkout.use_database') === true) {
            /**
             * @todo Add database support
             */
            throw new CheckoutException('Database is not supported yet.');
        } else {
            $file = \Storage::get(config('checkout.table_prefix') . 'Transactions');
            $update = false;
            foreach (explode(PHP_EOL, $file) as $line) {
                $data = explode('|.', $line);
                if ($data[0] === $transaction) {
                    $file = str_replace($line . PHP_EOL, '', $file);
                    $update = true;
                    break;
                }
            }

            if ($update)
                \Storage::put(config('checkout.table_prefix') . 'Transactions', $file);
        }
    }
}