<?php

namespace Kiberzauras\Checkout;

use Kiberzauras\Checkout\Lib\Customer;
use Kiberzauras\Checkout\Lib\Mode;
use Kiberzauras\Checkout\Lib\Product;
use Kiberzauras\Checkout\Lib\Provider;
use Kiberzauras\Checkout\Lib\CheckoutException;
use Kiberzauras\Checkout\Lib\TransactionData;

/**
 * Class Transaction
 * @package Kiberzauras\Checkout
 */
class Transaction {

    protected $transaction;
    protected $status = TransactionData::CREATED;
    protected $date_created;
    protected $date_updated;
    protected $date_paid;

    protected $amount;
    protected $tax_amount;
    protected $discount_amount;
    protected $currency;
    protected $description;
    /**
     * @var Product[]
     */
    protected $product_list = [];
    protected $customer_data = [];
    protected $provider;
    protected $mode;
    protected $transaction_id;
    protected $recurring_setup = [];
    protected $callback_url = null;
    protected $accept_url = null;
    protected $cancel_url = null;

    /**
     * @var Transaction
     */
    protected $request;

    /**
     * @param int $provider
     * @param int $mode
     * @param null $transaction_id
     */
    public function __construct($provider = Provider::PAYPAL, $mode = Mode::LIVE, $transaction_id = null)
    {
        $this->provider = $provider;
        $this->mode     = config('checkout.force_sandbox') ? Mode::SANDBOX : $mode;
        $this->currency = config('checkout.default_currency');
        $this->transaction_id = $transaction_id;
        $this->date_created = time();
        $this->date_updated = time();
    }

    /**
     * @param int $mode
     * @return $this
     */
    public function setMode($mode = Mode::LIVE)
    {
        $this->mode = $mode;
        return $this;
    }

    /**
     * @param int $provider
     * @return $this
     */
    public function setProvider($provider = Provider::PAYPAL)
    {
        $this->provider = $provider;
        return $this;
    }
    /**
     * @param float $amount
     * @param string $currency
     * @return $this
     */
    public function setAmount($amount = 0.00, $currency = null)
    {
        $this->amount = is_numeric($amount) ? $amount : 0.00;
        $this->currency = $currency ? $currency : config('checkout.default_currency');
        return $this;
    }

    /**
     * @param null $currency
     * @return $this
     */
    public function setCurrency($currency = null)
    {
        $this->currency = $currency ? $currency : config('checkout.default_currency');
        return $this;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description = '')
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @param Product $product
     * @return $this
     * @throws CheckoutException
     */
    public function addProduct(Product $product)
    {
        if (!$product->getName())
            throw new CheckoutException('Product name required');

        if (!$product->getAmount())
            throw new CheckoutException('Product amount (price) required');

        if (!$product->getQuantity())
            throw new CheckoutException('Product quantity required');

        if (!$product->getName())
            throw new CheckoutException('Product name required');

        $this->product_list[] = $product;
        return $this;
    }

    /**
     * @param Customer $customer
     * @return $this
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer_data = (object) $customer->expose();
        return $this;
    }

    /**
     * Setup billing payments, for now only PayPal payments
     * @param $startDate
     * @param $interval
     * @param $cycles
     * @return $this
     */
    public function setupRecurring($startDate, $interval, $cycles)
    {
        $this->recurring_setup = [
            'startDate' =>  strtotime($startDate),
            'interval'  =>  $interval,
            'cycles'    =>  $cycles
        ];
        return $this;
    }

    /**
     * @param null $callback_url
     * @param null $accept_url
     * @param null $cancel_url
     * @throws CheckoutException
     */
    public function pay($callback_url = null, $accept_url = null, $cancel_url = null)
    {
        $this->callback_url = $callback_url;
        $this->accept_url = $accept_url;
        $this->cancel_url = $cancel_url;

        if (!$this->accept_url)
            $this->accept_url = url('/');

        if (!$this->cancel_url)
            $this->cancel_url = url('/');

        if (count($this->product_list) && $this->amount > 0)
            throw new CheckoutException('Can\'t use setAmount() together with addProduct(), please choose only one.');

        if (!$this->currency)
            throw new CheckoutException('Currency code required');

        $expose = [];

        if (count($this->product_list))
            $expose = $this->handleProducts();

        if ($this->amount <= 0)
            throw new CheckoutException('Amount (price) must be greater than zero');

        if (count($this->recurring_setup))
            if (!in_array($this->provider, array(Provider::PAYPAL, Provider::BRAINTREE, Provider::GOOGLEWALLET)))
                throw new CheckoutException('Your selected provider cant serve subscriptions');

        $this->request = get_object_vars($this);
        $this->request['product_list'] = $expose;
        unset($this->request['request']);

        $this->request = TransactionData::generate($this->request);

        $gatewayClass = '\\Kiberzauras\\Checkout\\Gateway\\'. Provider::getGatewayName($this->provider) . "Gateway";
        (new $gatewayClass($this->request))
            ->startPayment();
    }

    /**
     *
     */
    protected function handleProducts()
    {
        $temp_amount = $temp_discount = $temp_tax = 0;
        $expose = [];
        foreach ($this->product_list as $product) {
            $tax        = @round(($product->getAmount() * $product->getQuantity()) / 100 * $product->getTaxRate(), 2) + ($product->getQuantity() * $product->getTaxValue());
            $discount   = @round(($product->getAmount() * $product->getQuantity()) / 100 * $product->getDiscountRate(), 2) + ($product->getQuantity() * $product->getDiscountValue());
            $temp_tax   += $tax;
            $temp_discount  += $discount;
            $temp_amount    += $product->getAmount() * $product->getQuantity();
            $expose[]   = $product->expose();
        }
        $this->tax_amount       = $temp_tax;
        $this->discount_amount  = $temp_discount;
        $this->amount   = $temp_amount;
        return $expose;
    }

}//Transaction
