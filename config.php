<?php
use \Kiberzauras\Checkout\Lib\Provider;

return array(
    /**
     *
     */
    'force_sandbox'     =>  false,

    /**
     *
     */
    'default_currency'  =>  'EUR',

    /**
     *
     */
    'route_prefix'  =>  'checkout',

    /**
     *
     */
    'enabled_logger'=>  false,
    'use_database'  =>  false,
    'table_prefix'  =>  'kiberzaurasCheckout',

    /**
     * Array of your live providers (on test payments system will look to sandbox_providers
     * and fallback here ir not found)
     */
    'providers'     =>  array(
        Provider::PAYPAL => array(
            'username'  =>  'inlu-business_api1.example.com',
            'password'  =>  '2PLJP7PNPNZHWFNE',
            'signature' =>  'AEyCxzNhwmlCEB7O76s6hNVrxOHkATQP48ofJMHlYcrUtj3p1HfhegVK'
        ),
        //we using 1.6 api version, change it in paysera.com project settings
        Provider::PAYSERA   => array(
            'projectId'     =>  '231',
            'signPassword'  =>  '95ce88f6e1a943a02dc50d516b298d',
            'accept'        =>  'lv_lpb,1stpay'
        )
    ),

    /**
     * Add all your sandbox accounts here
     */
    'sandbox_providers' =>  array(
        Provider::PAYPAL => array(
            'username'  =>  'inlu-business_api1.example.com',
            'password'  =>  '2PLJP7PNPNZHWFNE',
            'signature' =>  'AEyCxzNhwmlCEB7O76s6hNVrxOHkATQP48ofJMHlYcrUtj3p1HfhegVK'
        )
    )
);